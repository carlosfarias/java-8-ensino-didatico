package cap02;
public class ConversaoDeTipo {
	public static void main(String[] args) {
		String s1 = "10";
		System.out.println("S1: +" + s1);
		
		int v = Integer.parseInt(s1);
		System.out.println("V: " + v);
		
		float x = Float.parseFloat(s1);
		System.out.println("X: " + x);
		
		double y = Double.parseDouble(s1);
		System.out.println("Y: " + y);
		
		int w = (int) x;
		System.out.println("W: " + w);
		
		int z = (int) y;
		System.out.println("Z: " + z );
		
		String s2 = String.valueOf(v);
		System.out.println(s2 + w + z);
	}
}
